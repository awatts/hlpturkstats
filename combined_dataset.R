library(readr)
library(dplyr)
library(magrittr)
library(lubridate)
library(stringr)
# library(ggplot2)
library(uuid)

crossling <- read_csv(here::here('crosslinguistic_rawsubjects-2018-05-31.csv'))
crossling <- crossling[c('workerid', 'Experiment', 'Experimenter', 'title', 'assignmentid', 'hitid', 'hittypeid',
                         'creationtime', 'assignmentaccepttime', 'assignmentsubmittime', 'Age', 'Sex', 'Race', 'Ethnicity')]
crossling$protocol <- 'Crosslinguistic'

adaptation <- read_csv(here::here('adaptation_rawsubjects-2018-06-05.csv'))
adaptation <- adaptation[c('workerid','Experiment', 'Experimenter', 'title', 'assignmentid', 'hitid', 'hittypeid',
                           'creationtime', 'assignmentaccepttime', 'assignmentsubmittime', 'Age', 'Sex', 'Race', 'Ethnicity')]
adaptation$protocol <- 'Adaptation'

allhits <- rbind(crossling, adaptation) %>% distinct

allhits$hitid <- as.factor(allhits$hitid)
allhits$hittypeid <- as.factor(allhits$hittypeid)
allhits$assignmentid <- as.factor(allhits$assignmentid)
allhits$protocol <- as.factor(allhits$protocol)
allhits$Age <- as.integer(allhits$Age)
allhits$Sex <- as.factor(allhits$Sex)
allhits$Race <- as.factor(allhits$Race)
allhits$Ethnicity <- as.factor(allhits$Ethnicity)
# allhits$Experiment <- as.factor(allhits$Experiment)
allhits$title <- as.factor(allhits$title)

# I used this date in Python to indicate a bad value
bad_date = ymd_hms("1970-01-01 00:00:00")

anonyhits <- allhits %>%
  mutate(
    assignmentaccepttime = replace(assignmentaccepttime, assignmentaccepttime == bad_date, NA),
    assignmentsubmittime = replace(assignmentsubmittime, assignmentsubmittime == bad_date, NA),
    creationtime = replace(creationtime, creationtime == bad_date, NA)
  ) %>%
  group_by(workerid) %>%
  mutate(UUID = UUIDgenerate()) %>%
  mutate(HITcount = n()) %>%
  mutate(Year = year(assignmentsubmittime)) %>%
  mutate(Weekday = wday(assignmentsubmittime, label = TRUE)) %>%
  mutate(Hour = hour(assignmentaccepttime)) %>%
  mutate(Duration = as.duration(assignmentaccepttime %--% assignmentsubmittime)) %>%
  mutate(FirstSeen = min(assignmentaccepttime, na.rm = TRUE), LastSeen = max(assignmentaccepttime, na.rm = TRUE)) %>%
  ungroup() %>%
  select(-workerid)

anonyhits$UUID <- as.factor(anonyhits$UUID)
anonyhits$Year <- as.factor(anonyhits$Year)
anonyhits$Hour <- as.factor(anonyhits$Hour)

# Clean up some experiment names
anonyhits %<>% mutate(Experiment = case_when(
  str_detect(Experiment, 'BaeseberkGoldrickRep1.*') ~ 'BaeseBerkGoldrickRep1',
  str_detect(Experiment, 'Heller.Rep1.*') ~ 'Heller.Rep1',
  str_detect(Experiment, 'sz_context.*') ~ 'sz_context',
  str_detect(Experiment, 'needswashed') ~ 'NeedsWashed',
  # these two are based on finding NA experiments and comparing them to ones with same HITId or HITTypeId
  Experimenter == "Esteban Buz" & is.na(Experiment) ~ "BaeseBerkGoldrickRep1",
  Experimenter == "Masha Fedzechkina" & is.na(Experiment) & hittypeid == '2GAHRRLFQ0ONSVTLG98OEEGRBSTQH4' ~ "MultiDay",
  Experimenter == "Masha Fedzechkina" & is.na(Experiment) & hittypeid == '2JFIKMN9U8P933AXBLOI8SUYI7GTC5' ~ "IT Learn",
  Experimenter == "Linda Liu" & Experiment == "Unknown" ~ 'Kraljic Replication',
  TRUE ~ Experiment
))

write_csv(anonyhits, here::here('hlp_hits_anonymized.csv'))

# How many HITs do Turkers do?
#ggplot(anonyhits, aes(x = log(HITcount))) +
#  geom_histogram()

# show frequency of HIT acceptance by hour and day; size is proportion
# anonyhits <- anonyhits %>% filter(Year != 1970) # 1970-01-01 signals bad date
# ggplot(anonyhits, aes(Hour, Weekday)) +
#   geom_count(aes(size = ..prop.., group = 1)) +
#   facet_wrap(~ Year)
# ggsave(filename = 'hit_prop_by_year_day_hour', device = 'png')

# ditto, but for workers who did more than 10 HITs
# multitakes <- anonyhits %>% filter(HITcount > 10)
# ggplot(multitakes, aes(Hour, Weekday)) +
#  geom_count(aes(size = ..prop.., group = 1)) +
#  facet_wrap(~ Year)

