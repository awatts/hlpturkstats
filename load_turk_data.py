"""Read in MTurk result data and load it into Django DB."""

from typing import Tuple, Union

import numpy as np

import pandas as pd

from django.contrib.auth.models import User

from ua_parser import user_agent_parser

from hlp_mturk.models import Assignment, Browser, Experiment, ExperimentList, KeyWord, HIT, HITType, Protocol, DemographicSurvey, Worker

adapt = pd.read_csv('adaptation_rawsubjects-2018-08-16.csv', low_memory=False)
adapt['Protocol'] = 'Adaptation'
crossling = pd.read_csv('crosslinguistic_rawsubjects-2018-08-16.csv', low_memory=False)
crossling['Protocol'] = 'Crosslinguistic'

allhits = pd.merge(adapt, crossling, how='outer')

def merge_list(row: pd.core.series.Series) -> Union[str, np.float]: # np.nan is a float and sometimes lists are numbers
    """Merge `Answer.List` and `Answer.list`."""
    al = row['Answer.list'] if 'Answer.list' in row.keys() else np.nan
    aL = row['Answer.List'] if 'Answer.List' in row.keys() else np.nan

    rowvals = pd.Series((al, aL)).dropna().values

    if rowvals.size == 0:
        return np.nan
    elif rowvals.size == 1:
        return rowvals[0]
    else:
        return rowvals

def split_keys(row: pd.core.series.Series) -> Union[Tuple[str, ...], np.float]:
    """Split the keywords string into an array or return NaN to indicate empty."""
    return tuple([x.strip() for x in row['keywords'].split(',')]) if isinstance(row['keywords'], str) else np.nan


def add_os_browser(row: pd.core.series.Series) -> Union[pd.Series, np.float]:
    """Parse the browser string and return the browser and OS names"""
    if isinstance(row['Browser'], str):
        os = user_agent_parser.ParseOS(row['Browser'])
        browser = user_agent_parser.ParseUserAgent(row['Browser'])
        os_version = f"{os['major']}.{os['minor']}" if os['family'] == 'Mac OS X' else os['major']
        return pd.Series([os['family'], os_version, browser['family'], browser['major']])
    else:
        return np.nan

# Get the possible list columns into just one
allhits['List'] = allhits.apply(merge_list, axis=1)
# And split the keywords
allhits['keylist'] = allhits.apply(split_keys, axis=1)
# Parse the browser info and save that into new columns
allhits[['OSFamily', 'OSVersion', 'BrowserFamily', 'BrowserVersion']] = allhits.apply(add_os_browser, axis=1)

# Process unique workers
for worker in allhits.workerid.unique():
    obj, created = Worker.get_or_create(workerid=worker)

# Add all their unique surveys
survey_data = allhits.loc[:, ('assignmentsubmittime', 'workerid', 'Race', 'Sex', 'Ethnicity')]
survey_data.sort_values(by=['assignmentsubmittime', 'workerid'], inplace=True)
survey_data.drop_duplicates(['workerid', 'Sex', 'Race', 'Ethnicity'], inplace=True)
survey_data['submit_dt'] = pd.to_datetime(survey_data['assignmentsubmittime'])
# Select any row where at least one column is defined
has_survey = survey_data.query('(Race != "Unknown or Not Reported") | (Sex in ("Male", "Female")) | (Ethnicity in ("Hisp", "NonHisp"))')

SEX_OPTIONS = {
    'Male': 'M',
    'Female': 'F',
}

RACE_OPTIONS = {
    'White': 'white',
    'Other': 'unk',
    'Unknown or Not Reported': 'unk',
    'Black or African American': 'black',
    'More Than One Race': 'multi',
    'Asian': 'asian',
    'American Indian / Alaska Native': 'amerind',
    'Native Hawaiian or Other Pacific Islander': 'pacif'
}

for index, survey in has_survey.iterrows():
    worker = Worker.get(workerid=survey['workerid'])
    sex = 'NA' if not isinstance(survey['Sex'], str) else SEX_OPTIONS[survey['Sex']]
    eth = 'NA' if not isinstance(survey['Ethnicity'], str) else survey['Ethnicity']
    race = 'unk' if not isinstance(survey['Race'], str) else RACE_OPTIONS[survey['Ethnicity']]
    date = survey['submit_dt'].date()

    survey = DemographicSurvey(worker=worker, sex=sex, ethnicity=eth, race=race, date=date)

# Make sure the Protocols have been added; need it as foreign key for Experiment
for protocol in (
        {'number': 'RSRB00024018', 'shortname':'Crosslinguistic',
         'name': 'Cross-linguistic Studies on Human Communication'},
        {'number': 'RSRB00045955', 'shortname': 'Adaptation',
         'name': 'Linguistic Adaptation in a Variable Environment'}
    ):
    obj, created = Protocol.get_or_create(**protocol)

# Add the experimenters - this is done outside and then just username to full name mapping here
experimenter_usernames = {
    'Dave Kleinschmidt': 'dkleinschmidt',
    'Linda Liu': 'lliu',
    'Dan Pontillo': 'dpontillo',
    'Tal Linzen': 'tlinzen',
    'Maryam Seifeldin': 'mseifeldin',
    'Wednesday Bushong': 'wbushong',
    'Klinton Bicknell': 'kbicknell',
    'Ilker Yildirim': 'iyildirim',
    'Zach Burchill': 'zburchill',
    'Scott Fraundorf': 'sfraundorf',
    'Xin Xie': 'xxie',
    'BCS206 Group': 'bcs206',
    'Alex Fine': 'afine',
    'Jeremy Ferris': 'jferris',
    'Neal Snider': 'nsnider',
    'Judith Degen': 'jdegen',
    'Jackie Gutman': 'jgutman',
    'Thomas Farmer': 'tfarmer',
    'Masha Fedzechkina': 'mfedzech',
    'Florian Jaeger': 'fjaeger',
    'Nicole Craycraft': 'ncraycraft',
    'Esteban Buz': 'ebuz',
    'Kodi Weatherholtz': 'kweatherholtz',
    'Camber Hansen Carr': 'chansen',
    'Bozena Pajak': 'bpajak',
    'Ben Anible': 'banible',
    'Shaorong Yan': 'syan'
}

# Clean up bad experiment names in original data
allhits.loc[(allhits.Experiment.str.match('BaeseberkGoldrickRep1.*')), 'Experiment'] = 'BaeseberkGoldrickRep1'
allhits.loc[(allhits.Experiment.str.match('Heller.Rep1.*')), 'Experiment'] = 'Heller.Rep1'
allhits.loc[(allhits.Experiment.str.match('sz_context.*')), 'Experiment'] = 'sz_context'
allhits.loc[allhits.Experiment == 'needswashed', 'Experiment'] = 'NeedsWashed'
allhits.loc[(allhits.Experimenter == 'Esteban Buz') & (allhits.Experiment.isnull()), 'Experiment'] = 'BaeseberkGoldrickRep1'
allhits.loc[(allhits.Experimenter == 'Masha Fedzechkina') & (allhits.Experiment.isnull()) & (allhits.hittypeid == '2GAHRRLFQ0ONSVTLG98OEEGRBSTQH4'), 'Experiment'] = 'MultiDay'
allhits.loc[(allhits.Experimenter == 'Masha Fedzechkina') & (allhits.Experiment.isnull()) & (allhits.hittypeid == '2JFIKMN9U8P933AXBLOI8SUYI7GTC5'), 'Experiment'] = 'IT Learn'
allhits.loc[(allhits.Experimenter == 'Linda Liu') & (allhits.Experiment == 'Unknown'), 'Experiment'] = 'Kraljic Replication'

# Add the experiments - TODO: memoize this
all_experiments = allhits[['Experiment', 'Experimenter', 'Protocol']].drop_duplicates().dropna()
for index, row in all_experiments.iterrows():
    experimenter = User.objects.get_by_natural_key(experimenter_usernames[row['Experimenter']])
    protocol = Protocol.objects.get(shortname=row['Protocol'])
    experiment = Experiment.objects.get_or_create(name=row['Experiment'],
                                                  experimenter=experimenter,
                                                  protocol=protocol)

all_ex_lists = allhits[['Experiment', 'List']].drop_duplicates().dropna()
for index, row in all_ex_lists.iterrows():
    experiment = Experiment.objects.get(name=row['Experiment'])
    ex_list = ExperimentList(name=row['List'], experiment=experiment)

# Process keywords
keylists = allhits['keylist'].dropna()
all_keywords = set([y for x in keylists.tolist() for y in x])
for word in all_keywords:
    kw = KeyWord(word=word)

# Process HITTypeIDs - remember Experiment could be NaN
all_hittypeids = allhits[['hittypeid', 'Experiment']].drop_duplicates().dropna()  # FIXME: dropna drops if any na. check that this doesn't have bad side effect
for index, row in all_hittypeids.iterrows():
    experiment = Experiment.objects.get(name=row['Experiment'])
    hittypeid = None if not isinstance(all_hittypeids['hittypeid'], str) else all_hittypeids['hitttypeid']
    htid = HITType(experiment=experiment, hittypeid=hittypeid)

# Process HITIds - weirdly, 'assignmentduration' is part of HIT not an Assignment
STATUS_OPTIONS = {
    'Available': 'AV',
    'Unassignable': 'UN',
    'Reviewable': 'RB',
    'Reviewing': 'RG',
    'Disposed': 'DI'
}
all_hits = allhits[['hitid', 'title', 'description', 'annotation', 'hitstatus', 'hitlifetime', 'creationtime', 'hittypeid', 'keylist', 'assignmentduration']].drop_duplicates()
for index, row in all_hits.iterrows():
    if not isinstance(row['hitid'], str):
        continue  # only care about rows where HITId is defined
    else:
        hitid = row['hitid']
        title = None if not isinstance(row['title'], str) else row['title']
        description = None if not isinstance(row['description'], str) else row['description']
        annotation = None if not isinstance(row['annotation'], str) else row['annotation']
        hitstatus = None if not isinstance(row['hitstatus'], str) else STATUS_OPTIONS[row['hitstatus']]
        hitlifetime = None  # FIXME: leaving empty for all for now. Should be integer seconds but is time stamp or null
        creationtime = None if not isinstance(row['creationtime'], str) else pd.to_datetime(row['creationtime']).to_pydatetime()
        hittypeid = None if not isinstance(row['hittypeid'], str) else HITType.objects.get(row['hittypeid'])
        assignmentduration = None if not isinstance(row['assignmentduration'], str) else int(row['assignmentduration'])

        hit = HIT(hitid=hitid, title=title, description=description, annotation=annotation,
                  hitstatus=hitstatus, creationtime=creationtime, hittypeid=hittypeid,
                  duration=assignmentduration)

        if isinstance(row['keylist'], tuple):
            for k in row['keylist']:
                hit.keywords_set.add(KeyWord.objects.get(word=row['k']))


# Process Assignments
all_assignments = allhits[['assignmentid', 'assignmentaccepttime', 'assignmentsubmittime',
                           'assignmentapprovaltime', 'assignmentrejecttime', 'autoapprovaltime',
                           'deadline', 'feedback', 'assignmentstatus', 'workerid', 'hitid']].drop_duplicates()

ASSIGNMENT_STATUS = {
    'Submitted': 'SUB',
    'Approved': 'APP',
    'Rejected': 'REJ'
}

for index, row in all_assignments.iterrows():
    if not isinstance(row['assignmentid'], str):
        continue  # only care about rows where AssignmentId is defined
    else:
        assignmentid = row['assignmentid']
        assignmentaccepttime = None if not isinstance(row['assignmentaccepttime'], str) else pd.to_datetime(row['assignmentaccepttime']).to_pydatetime()
        assignmentsubmittime = None if not isinstance(row['assignmentsubmittime'], str) else pd.to_datetime(row['assignmentsubmittime']).to_pydatetime()
        assignmentapprovaltime = None if not isinstance(row['assignmentapprovaltime'], str) else pd.to_datetime(row['assignmentapprovaltime']).to_pydatetime()
        assignmentrejecttime = None if not isinstance(row['assignmentrejecttime'], str) else pd.to_datetime(row['assignmentrejecttime']).to_pydatetime()
        autoapprovaltime = None if not isinstance(row['autoapprovaltime'], str) else pd.to_datetime(row['autoapprovaltime']).to_pydatetime()
        deadline = None  # doesn't seem to be defined by any existing results files
        feedback = None if not isinstance(row['feedback'], str) else row['feedback']
        assignmentstatus = None if not isinstance(row['assignmentstatus'], str) else ASSIGNMENT_STATUS[row['assignmentstatus']]
        workerid = None if not isinstance(row['workerid'], str) else Worker.objects.get(row['workerid'])
        hitid = None if not isinstance(row['hitid'], str) else Worker.objects.get(row['hitid'])

browser_info = allhits[['OSFamily', 'OSVersion', 'BrowserFamily', 'BrowserVersion']].drop_duplicates().dropna()
for index, row in browser_info.iterrows():
    assignments = allhits[
        (allhits.OSFamily==row['OSFamily']) &
        (allhits.OSVersion==row['OSVersion']) &
        (allhits.BrowserFamily==row['BrowserFamily']) &
        (allhits.BrowserVersion==row['BrowserVersion'])
    ]['assignmentid']

    browser = Browser(browser=row['BrowserFamily'],
                      version=row['BrowserVersion'],
                      os=row['OSFamily'],
                      os_version=row['OSVersion']
                     )
    for i, x in assignments.iteritems():
        browser.assignment_set.add(Assignment.objects.get(x))
