#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import pandas as pd
import argparse
from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

CSI = "\x1B["
reset = CSI+"m"

parser = argparse.ArgumentParser(
    description='Load one or more MTurk results files, merge them and '
                'deduplicate the rows.')
parser.add_argument('-r', '--resultsfilelist', required=True,
                    help='(required) YAML file with list of results files to use')
parser.add_argument('-o', '--overwriteexperiment', action='store_true',
                    help='Replace existing Answer.[Ee]xperiment with YAML file "experiment"')
args = parser.parse_args()

with open(args.resultsfilelist, 'r') as rfile:
    expdata = load(rfile, Loader=Loader)

abort = False
for k in ('resultsfiles', 'delimiter', 'outfile'):
    if k not in expdata:
        print('{} is a required key in HIT file!'.format(k))
        abort = True
if abort:
    print('At least one required key missing; aborting HIT load')
    import sys
    sys.exit()

resfiles = expdata['resultsfiles']
outfile = expdata['outfile']
delim = '\t' if expdata['delimiter'] == 'tab' else ','

columns_of_interest = {
    'hitid', 'HitId',
    # Some web UI downloaded files are missing 'HITTypeId' or 'hittypeid'
    'hittypeid', 'HITTypeId',
    'title', 'Title', 'HitTitle',
    'description', 'Description',
    'keywords', 'Keywords',
    'reward', 'Reward',
    'creationtime', 'CreationTime',
    'assignments', 'MaxAssignments',
    'numavailable', 'NumberofAssignmentsAvailable',
    'numpending', 'NumberofAssignmentsPending',
    'numcomplete', 'NumberofAssignmentsCompleted',
    'hitstatus', 'HITStatus',
    'reviewstatus', 'HITReviewStatus',
    'annotation', 'RequesterAnnotation'
    'assignmentduration', 'AssignmentDurationInSeconds',
    'autoapprovedelay', 'AutoApprovalDelayInSeconds',
    'hitlifetime', 'LifetimeInSeconds',
    'viewhit',
    'assignmentid', 'AssignmentId',
    'workerid', 'WorkerId',
    'assignmentstatus', 'AssignmentStatus',
    'assignmentaccepttime', 'AcceptTime',
    'assignmentsubmittime', 'SubmitTime',
    'assignmentrejecttime', 'RejectionTime',
    'deadline',
    'feedback',
    'reject',
    'Answer.experiment', 'Answer.Experiment',
    'Answer.list', 'Answer.List',
    'Answer.browser', 'Answer.browserid', 'Answer.Browser', 'Answer.userAgent',
    'Answer.rsrb.raceother',
    'Answer.rsrb.ethnicity',
    'Answer.rsrb.sex',
    'Answer.rsrb.age',
    # Ilker has a column for each race, others just have "Answer.rsrb.race"
    'Answer.rsrb.race.amerind',
    'Answer.rsrb.race.asian',
    'Answer.rsrb.race.black',
    'Answer.rsrb.race.other',
    'Answer.rsrb.race.pacif',
    'Answer.rsrb.race.unknown',
    'Answer.rsrb.race.white',
    'Answer.rsrb.race'
}

name_map = {'HITId': 'hitid', 'HitId': 'hitid',
            'HITTypeId': 'hittypeid',
            'Title': 'title', 'HitTitle': 'title',
            'Description': 'description',
            'Keywords': 'keywords',
            'Reward': 'reward',
            'CreationTime': 'creationtime',
            'MaxAssignments': 'assignments',
            'NumberofAssignmentsAvailable': 'numavailable',
            'NumberofAssignmentsPending': 'numpending',
            'NumberofAssignmentsCompleted': 'numcomplete',
            'HITStatus': 'hitstatus',
            'HITReviewStatus': 'reviewstatus',
            'RequesterAnnotation': 'annotation',
            'AssignmentDurationInSeconds': 'assignmentduration',
            'AutoApprovalDelayInSeconds': 'autoapprovedelay',
            'LifetimeInSeconds': 'hitlifetime',
            'AssignmentId': 'assignmentid',
            'WorkerId': 'workerid',
            'AssignmentStatus': 'assignmentstatus',
            'AcceptTime': 'assignmentaccepttime',
            'SubmitTime': 'assignmentsubmittime',
            'RejectionTime': 'assignmentrejecttime',
            }

results = None
for r in resfiles:
    with open(r['file'], 'r') as resfile:
        # We're only keeping the columns that will be of interest when generating
        # the demographic data / subject count report
        print('Loading {}'.format(r['file']))
        rdf = pd.read_csv(resfile, delimiter=delim, low_memory=False, parse_dates=False)

        coi = columns_of_interest.intersection(rdf.columns)

        rename_keys = coi.intersection(name_map.keys())
        renames = {x[0]: x[1] for x in name_map.items() if x[0] in rename_keys}

        # Some really old ones have no demographic data
        # Color info from http://stackoverflow.com/a/21786287/3846301
        if 'Answer.rsrb.ethnicity' not in coi:
            print(CSI + "31;40m" + u"\u2717" + CSI + "0m" + "\t{} has no demographic information".format(resfile.name.split('/')[-1]))
        else:
            print(CSI + "32;40m" + u"\u2713" + CSI + "0m" + "\t{} has demographic information".format(resfile.name.split('/')[-1]))

        try:
            results_selected = rdf.loc[:, coi]
        except KeyError as e:
            print('KeyError: {}'.format(e))
            print('Columns of interest: {}'.format(coi))
            print('Actual columns: {}'.format(rdf.columns))

        # This is useful if you need to figure out which files are problematic
        # but if you don't comment it out, you can end up with duplicates
        # results_selected.loc[:, 'filename'] = resfile.name

        if 'WorkerId' in coi:
            # print('Renaming columns: {}'.format(rename_keys))
            results_selected.rename(columns=renames, inplace=True)

        if args.overwriteexperiment:
            to_drop = list(set(['Answer.experiment', 'Answer.Experiment']) & set(results_selected.columns))
            if to_drop:
                results_selected.drop(columns=to_drop, inplace=True)
            results_selected['Experiment'] = r['name']
            results_selected['Experimenter'] = r['experimenter']

        if results is None:
            results = results_selected
        else:
            results = results.append(results_selected)

print('Starting with {} rows.'.format(len(results)))
results.drop_duplicates(inplace=True)
print('After removing duplicates there are {} rows.'.format(len(results)))

# If we're going to filter by date, we need to convert submittimes to dates
if 'filter' in expdata:
    from dateutil.parser import parse

    datecap = expdata['filter']['date']
    dateop = expdata['filter']['operator']

    def fixsubmittime(row):
        return parse(row['assignmentsubmittime']).date()

    results['SubmitDate'] = results.apply(fixsubmittime, axis=1)

    if dateop == 'before':
        results = results.loc[results['SubmitDate'] < datecap]

    if dateop == 'after':
        results = results.loc[results['SubmitDate'] > datecap]

    print('After removing date nonmatches there are {} rows.'.format(len(results)))

# results['assignmentsubmittime'] = pd.to_datetime(results['assignmentsubmittime'])
# crosssubset = results.loc[results['assignmentsubmittime'] < datecap]

results.to_csv(path_or_buf='{}'.format(outfile), sep='\t', index=False)
